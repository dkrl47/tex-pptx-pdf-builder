# tex-pptx-pdf-builder

Tools to build office and tex to pdf in GitLab CI pipeline.
Github Action support is planned.

All source tex files need to be listed in `tex/Makefile`
All office files in `pptx/Makefile`. All file formats supported in libreoffice may be used.

Build files are saved as job artifacts in Gitlab and may be downloaded here:

## Download Tex

- [TEX-PDF](https://git.thm.de/dkrl47/tex-pptx-pdf-builder/-/jobs/artifacts/main/raw/tex/Beispiel.pdf?job=praktikum-tex)

## Download PPTX

- [Ganze Präsi](https://git.thm.de/dkrl47/tex-pptx-pdf-builder/-/jobs/artifacts/main/raw/pptx/Lorem-Ipsum.pdf?job=vorlesung-pptx)
- [Seite 1](https://git.thm.de/dkrl47/tex-pptx-pdf-builder/-/jobs/artifacts/main/raw/pptx/Lorem-Ipsum-1.pdf?job=vorlesung-pptx)
- [Seite 2](https://git.thm.de/dkrl47/tex-pptx-pdf-builder/-/jobs/artifacts/main/raw/pptx/Lorem-Ipsum-2.pdf?job=vorlesung-pptx)
